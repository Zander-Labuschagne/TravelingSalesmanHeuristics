function [x, y] = find_first_city(image_plot)
%FIND_FIRST_CITY Summary of this function goes here
%   Detailed explanation goes here
%% Finding first city
	fprintf('Finding the first pixel...\n');
	city_found = false;
	for ii = 1 : size(image_plot, 2)
		for iii = 1 : size(image_plot, 1)
			if (image_plot(ii, iii) == 0)
				x = ii;
				y = iii;
				city_found = true;
				break;
			end;
		end;
		if (city_found)
			break;
		end;
	end;
end

