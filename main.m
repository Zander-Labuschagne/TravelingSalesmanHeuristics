clear;
image = imread('images/bart.jpg');
%image = imresize(image, 0.5);
fprintf('Image resolution: %dx%d\n', size(image, 1), size(image, 2));
image_bw = imbinarize(image);
image_bw = flipdim(imrotate(image_bw, 180), 2);
% imshow(image_bw);

%% Get random pixels
fprintf('Picking random pixels...\n');
number_of_pixels = 200;
image_reduced = ones(size(image_bw, 1), size(image_bw, 2));
x = 1;
y = 1;
for i = 1 : number_of_pixels
	value = 1;
	pixel_taken = false;
	while (value ~= 0 && ~pixel_taken)
		y = randi(size(image_bw, 2));
		x = randi(size(image_bw, 1));
		if (image_reduced(x, y) ~= 0)
			value = image_bw(x, y);
			pixel_taken = false;
		else
			pixel_taken = true;
		end;
	end;
	image_reduced(x, y) = 0;
end;

figure;
map = plot(NaN, NaN);
hold on;
fprintf('Plotting cities...\n');
%imshow(image_reduced); % Can use this instead of plotting like below,
%better option but not in the case of animation
%% Plot the pseudo random cities
for xi = 1 : size(image_reduced, 1)
	for xii = 1 : size(image_reduced, 2)
		if (image_reduced(xi, xii) == 0)
			plot(xii, xi, '.r');
		end;
	end;
end;
drawnow;
saveas(map, 'image.cities.png');
%% Find nearest neighbours
%[x_list, y_list] = get_nearest_neighbour_route(image_reduced, number_of_pixels)
window_size = 3;
visited = zeros(size(image_reduced, 1), size(image_reduced, 2));
list_counter = 1;
x_list = zeros(1, number_of_pixels);
y_list = zeros(1, number_of_pixels);
[x, y] = find_first_city(image_reduced);
%x_list(1) = x
%y_list(1) = y
while (true)
	nn_found = false;
	for iv = (y - floor(window_size / 2)) : y + floor(window_size / 2)
		for v = x - floor(window_size / 2) : x + floor(window_size / 2)
			%% Ensuring window is within image bounds for both axis
			if (iv < 1)
				iv_i = 1;
			elseif (iv > size(image_reduced, 2))
				iv_i = size(image_reduced, 2);
			else
				iv_i = iv;
			end;
			if (v < 1)
				v_i = 1;
			elseif (v > size(image_reduced, 1))
				v_i = size(image_reduced, 1);
			else
				v_i = v;
			end;

			%% If neighbour is found
			if (image_reduced(v_i, iv_i) == 0 && visited(v_i, iv_i) == 0 && iv_i ~= y && v_i ~= x)
				%fprintf('Neighbour found!\n');
				nn_y = iv_i;
				nn_x = v_i;
				visited(nn_x, nn_y) = 1;

				% add line to list of lines to draw later
				y_list(1, list_counter) = nn_y;
				x_list(1, list_counter) = nn_x;

				list_counter = list_counter + 1;

				nn_found = true;
				y = nn_y;
				x = nn_x;
				window_size = 3;
				break;
			end;
		end;
		if (nn_found)
			break;
		end;
	end;
	if (nn_found == false)
		window_size = window_size + 2;
	end;
	if (list_counter - 1 == number_of_pixels || window_size > size(image, 1))
		break;
	end;
end;

%% Trimming coordinates of zeros
x_list = x_list(find(x_list, 1, 'first'):find(x_list, 1, 'last'));
y_list = y_list(find(y_list, 1, 'first'):find(y_list, 1, 'last'));

%% Plotting route
fprintf('Plotting route...\n');
pbaspect([1 1 1]);
p = plot(y_list, x_list, 'Color', 'b', 'LineWidth', 1); % One shot plot
saveas(map, 'image.png');
set(p,'Visible','off')
%% Animation
for xiii = 2 : length(x_list)
	pause(0.1);
	if (xiii > 2)
		%plot(y_list(1:xiii - 1), x_list(1:xiii - 1), 'Color', 'b', 'LineWidth', 1);
		set(map, 'XData', y_list(1:xiii - 1), 'YData', x_list(1:xiii - 1));
		%refreshdata;
		%drawnow;
	end;
	%% Big chunk of code below is to animate the straight lines or travels
	if (abs(y_list(xiii - 1) - y_list(xiii)) > abs(x_list(xiii - 1) - x_list(xiii)))
		delta = abs(y_list(xiii - 1) - y_list(xiii));
	else
		delta = abs(x_list(xiii - 1) - x_list(xiii));
	end;
	y_line = linspace(y_list(xiii - 1), y_list(xiii), delta);
	x_line = linspace(x_list(xiii - 1), x_list(xiii), delta);
	for xiv = 1 : delta
		pause(0.001);
		y_list(xiii) = y_line(xiv);
		x_list(xiii) = x_line(xiv);
		set(map, 'XData', y_list(1:xiii), 'YData', x_list(1:xiii));
		%% Create an animated gif
% 		frame = getframe(1);
% 		im = frame2im(frame);
% 		[imind,cm] = rgb2ind(im,256);
% 		if (xiii == 2)
% 			imwrite(imind,cm,'preview','gif','Loopcount',inf);
% 		else
% 			imwrite(imind,cm,'preview','gif','WriteMode','append');
% 		end;
	end;
end;