# TravelingSalesmanHeuristics

Implementations of heuristics to solve the traveling salesman problem using binary images

## Some previews
<img src="https://gitlab.com/Zander-Labuschagne/TravelingSalesmanHeuristics/raw/master/screens/image2000.dots.png" alt="Showing 2000 cities to be visited" width="300" align="middle"> <br />

<img src="https://gitlab.com/Zander-Labuschagne/TravelingSalesmanHeuristics/raw/master/screens/image2000.png" alt="Showing 2000 cities visited and connected" width="300" align="middle"> <br />

<img src="https://gitlab.com/Zander-Labuschagne/TravelingSalesmanHeuristics/raw/master/screens/preview100" alt="Showing 100 animated cities visited" width="300" align="middle"> <br />

<img src="https://gitlab.com/Zander-Labuschagne/TravelingSalesmanHeuristics/raw/master/screens/preview10" alt="Showing 10 animated cities visited" width="300" align="middle"> <br />