#include <math.h>

#include <iostream>

#include <opencv2/opencv.hpp>


int main()
{
	cv::Mat image = cv::imread("images/bart.jpg", 0);

	if (image.empty()) {
		std::cout << "!!! Failed imread(): image not found" << std::endl;
		return -1;
	}
	
	cv::Mat image_bw;
	cv::threshold(image, image_bw, 128, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
	
	// Get random pixels
	std::cout << "Picking random pixels... \n" << std::endl;
	const unsigned short NUMBER_OF_PIXELS = 4000;
	cv::Mat image_reduced = cv::Mat::zeros(image_bw.rows, image_bw.cols, image_bw.type());
	// Making all background pixels white
	for (unsigned short m = 0; m < image_reduced.rows; ++m)
		for (unsigned short n = 0; n < image_reduced.cols; ++n)
			image_reduced.at<cv::Vec3b>(cv::Point(m, n)).val[2] = 255;
	unsigned short x = 0;
	unsigned short y = 0;
	std::cout << std::endl << 'X' << "    " << 'Y' << std::endl;
	srand (time(NULL));
	for (unsigned short i = 0; i < NUMBER_OF_PIXELS; ++i) {
		unsigned short value = 255;
		bool pixel_taken = true;
		while (pixel_taken) {
			x = rand() % image_bw.rows;
			y = rand() % image_bw.cols;
			if (image_reduced.at<cv::Vec3b>(cv::Point(x, y)).val[2] == 255 && image_bw.at<cv::Vec3b>(cv::Point(x, y)).val[2] == 0) {
				pixel_taken = false;
				std::cout << x << "    " << y << std::endl;
				image_reduced.at<cv::Vec3b>(cv::Point(x, y)).val[2] = 0;
			} else
				pixel_taken = true;
		}
	}
	
	// Finding first pixel
	std::cout << "Finding the first pixel..." << std::endl;
	for (unsigned short ii = 0; ii < image_reduced.rows; ++ii)
		for (unsigned short iii = 0; iii < image_reduced.cols; ++iii)
			if (image_reduced.at<cv::Vec3b>(cv::Point(ii, iii)).val[2] == 0) {
				x = ii;
				y = iii;
			}
	std::cout << x << "    " << y << std::endl;
			
	// Find nearest neighbours
	std::cout << "Finding nearest neighbours..." << std::endl;
	unsigned short window_size = 3;
	//cv::Mat visited = cv::Mat::zeros(image_reduced.rows, image_reduced.cols);
	bool visited[image_reduced.rows][image_reduced.cols];
	for (unsigned short viii = 0; viii < image_reduced.rows; ++viii)
		for (unsigned short ix = 0; ix < image_reduced.cols; ++ix)
			visited[viii][ix] = false;
	unsigned int vector_index_counter = 0;
	unsigned int x_vector[NUMBER_OF_PIXELS];
	unsigned int y_vector[NUMBER_OF_PIXELS];
	for (unsigned int vii = 0; vii < NUMBER_OF_PIXELS; ++vii) {
		x_vector[vii] = 0;
		y_vector[vii] = 0;
	}
	while (true) {
		bool nn_found = false;
		for (short iv = x - floor(window_size / 2); iv <= x + floor(window_size / 2); ++iv) {
			for (short v = y - floor(window_size / 2); v <= y + floor(window_size / 2); ++v) {
				// Ensuring window is within image bounds for both axis
				unsigned short iv_i;
				unsigned short v_i;
				if (iv < 0)
					iv_i = 0;
				else if (iv >= image_reduced.rows)
					iv_i = image_reduced.rows - 1;
				else
					iv_i = iv;
				if (v < 0)
					v_i = 0;
				else if (v >= image_reduced.cols)
					v_i = image_reduced.cols - 1;
				else
					v_i = v;
				
				// If neighbour is found
				if (image_reduced.at<cv::Vec3b>(cv::Point(iv_i, v_i)).val[2] == 0 && !visited[iv_i][v_i] /*&& v_i != y && iv_i != x*/) {
					unsigned short nn_y = v_i;
					unsigned short nn_x = iv_i;
					visited[nn_x][nn_y] = true;
					//add line to list of lines to draw later
					y_vector[vector_index_counter] = nn_y;
					x_vector[vector_index_counter++] = nn_x;
					
					std::cout << x_vector[vector_index_counter - 1] << "    " << y_vector[vector_index_counter - 1] << std::endl;
					
					nn_found = true;
					y = nn_y;
					x = nn_x;
					window_size = 3;
					break;
				}
			}
			if (nn_found)
				break;
		}
		if (!nn_found)
			window_size += 4;
		if (vector_index_counter == NUMBER_OF_PIXELS /*|| window_size > (image_reduced.cols < image_reduced.rows ? image_reduced.cols : image_reduced.rows)*/)
			break;
	}
	
	// Plotting route
	std::cout << "Plotting route..." << std::endl;
	for (unsigned int vi = 0; vi < NUMBER_OF_PIXELS - 1; ++vi) {
		cv::line(image_reduced, cv::Point(x_vector[vi], y_vector[vi]), cv::Point(x_vector[vi + 1], y_vector[vi + 1]), 0, 1);
		std::cout << '[' << x_vector[vi] << ", " << y_vector[vi] << ']' << "  ->  " << '[' << x_vector[vi + 1] << ", " << y_vector[vi + 1] << "]" << std::endl;; 
	}
	
		
	cv::namedWindow("Reduced image", cv::WINDOW_AUTOSIZE);
	cv::imshow("Reduced image", image_reduced);
	cv::waitKey();

	//std::cout << image_bw.type() << " " << image_reduced.type() << std::endl;

	return 0;
}
