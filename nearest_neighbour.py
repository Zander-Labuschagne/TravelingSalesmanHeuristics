import matplotlib.pyplot as mpplot
import matplotlib.image as mpimage
import cv2
import random
import numpy
import math

image=mpimage.imread('images/bart.jpg');
#imgplot = mpplot.imshow(image)
image = cv2.resize(image, (0,0), fx=0.4, fy=0.4); 
#cv2.imshow("Original Image", image);
#cv2.waitKey(0);

(thresh, image_bw) = cv2.threshold(image, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
#cv2.imshow("Binarized Image", image_bw);
#cv2.waitKey();

# Get Random pixels
print('Picking random pixels...\n');
number_of_pixels = 100;
image_reduced = numpy.ones([image_bw.shape[0], image_bw.shape[1]]);
for i in range(0, number_of_pixels):
	value = 1;
	pixel_taken = False;
	while (value != 0 and not pixel_taken):
		y = random.randint(0, image_bw.shape[1] - 1);
		x = random.randint(0, image_bw.shape[0] - 1);
		if (image_reduced[x][y] != 0):
			value = image_bw[x][y];
			pixel_taken = False;
		else:
			pixel_taken = True;
	image_reduced[x][y] = value;

#[x, y] = find(binary_image);
#locations = [x, y];
#random.shuffle(locations);
#cv2.imshow("Reduced Image", image_reduced);
#cv2.waitKey();

# Finding first pixel
print('Finding the first pixel...\n');
for ii in range(0, image_reduced.shape[1]):
	for iii in range(0, image_reduced.shape[0]):
		if (image_reduced[iii][ii] == 0):
			x = iii;
			y = ii;

# Find nearest neighbours
print('Finding nearest neighbours...\n');
window_size = 3;
visited = numpy.zeros([image_reduced.shape[0], image_reduced.shape[1]]);
vector_index_counter = 0;
x_vector = numpy.zeros([number_of_pixels], dtype = int);
y_vector = numpy.zeros([number_of_pixels], dtype = int);
while True:
	nn_found = False;
	for iv in range(y - int(math.floor(window_size / 2)), y + int(math.floor(window_size / 2))):
		for v in range(x - int(math.floor(window_size / 2)), x + int(math.floor(window_size / 2))):
			# Ensuring window is within image bounds
			if (iv < 0):
				iv_i = 0;
			elif (iv >= image_reduced.shape[1]):
				iv_i = image_reduced.shape[1] - 1;
			else:
				iv_i = iv;
			if (v < 0):
				v_i = 0;
			elif (v >= image_reduced.shape[0]):
				v_i = image_reduced.shape[0] - 1;
			else:
				v_i = v;

			# If neighbour is found
			if (image_reduced[v_i][iv_i] == 0 and visited[v_i][iv_i] == 0 and iv_i != y and v_i != x):
				nn_y = iv_i;
				nn_x = v_i;
				visited[v_i][iv_i] = 1;
				y_vector[vector_index_counter] = nn_y;
				x_vector[vector_index_counter] = nn_x;
				vector_index_counter += 1;
				nn_found = True;
				y = nn_y;
				x = nn_x;
				window_size = 3;
				break;
		if (nn_found):
			break;
	if (not nn_found):
		window_size += 2;
	if (vector_index_counter - 1 == number_of_pixels or window_size > image_reduced.shape[0]):
		break;

# Trimming coordinates of zeros
numpy.trim_zeros(y_vector);
numpy.trim_zeros(x_vector);

# Plotting route
print('Plotting route...\n');
for vi in range(0, x_vector.shape[0] - 1):
	cv2.line(image_reduced, (y_vector[vi], x_vector[vi]), (y_vector[vi + 1], x_vector[vi + 1]), (0, 255, 0), 1);

cv2.imshow('Traveler', image_reduced);
cv2.waitKey();
