import matplotlib.pyplot as mpplot
import matplotlib.image as mpimage
import cv2
import random
import numpy
import math

image=mpimage.imread('images/bart.jpg');
#imgplot = mpplot.imshow(image)
image = cv2.resize(image, (0,0), fx=0.4, fy=0.4); 
#cv2.imshow("Original Image", image);
#cv2.waitKey(0);

(thresh, image_bw) = cv2.threshold(image, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
#cv2.imshow("Binarized Image", image_bw);
#cv2.waitKey();

# Get Random pixels
print('Picking random pixels...\n');
number_of_pixels = 100;
image_reduced = numpy.ones([image_bw.shape[0], image_bw.shape[1]]);
for i in range(0, number_of_pixels):
	value = 1;
	pixel_taken = False;
	while (value != 0 and not pixel_taken):
		y = random.randint(0, image_bw.shape[1] - 1);
		x = random.randint(0, image_bw.shape[0] - 1);
		if (image_reduced[x][y] != 0):
			value = image_bw[x][y];
			pixel_taken = False;
		else:
			pixel_taken = True;
	image_reduced[x][y] = value;

# Finding first pixel
print('Finding the first pixel...\n');
for ii in range(0, image_reduced.shape[1]):
	for iii in range(0, image_reduced.shape[0]):
		if (image_reduced[iii][ii] == 0):
			x = iii;
			y = ii;

# Find nearest neighbours
print('Finding nearest neighbours...\n');
window_size = 3;
visited = numpy.zeros([image_reduced.shape[0], image_reduced.shape[1]]);
vector_index_counter = 0;
x_vector = numpy.zeros([number_of_pixels], dtype = int);
y_vector = numpy.zeros([number_of_pixels], dtype = int);
while True:
	nn_found = False;

	for v in range(int(-math.floor(window_size / 2)), int(math.floor(window_size / 2))):
		if (x + v >= image_reduced.shape[0]):
			v_i_x = image_reduced.shape[0] - 1;
		elif (x - v < 0):
			v_i_x = 0;
		else:
			v_i_x = x + v;
		if (y + v >= image_reduced.shape[1]):
			v_i_y = image_reduced.shape[1];
		elif (y - v < 0):
			v_i_y = 0;
		else:
			v_i_y = y + v;
		if (x - math.floor(window_size / 2) < 0):
			fl_x = 0;
		else:
			fl_x = x - math.floor(window_size / 2);
		if (x + math.floor(window_size / 2) >= image_reduced.shape[0]):
			ceil_x = image_reduced.shape[0] - 1;
		else:
			ceil_x = x + math.floor(window_size / 2);
		if (y - math.floor(window_size / 2) < 0):
			fl_y = 0;
		else:
			fl_y = y - math.floor(window_size / 2);
		if (y + math.floor(window_size / 2) >= image_reduced.shape[1]):
			ceil_y = image_reduced.shape[1] - 1;
		else:
			ceil_y = y + math.floor(window_size / 2);

	# If neighbour is found
		if ((image_reduced[v_i_x][fl_y] == 0 or image_reduced[v_i_x][ceil_y] == 0 or image_reduced[fl_x][v_i_y] == 0 or image_reduced[ceil_x][v_i_y] == 0) and visited[v_i_x][v_i_y] == 0 and v_i_y != y and v_i_x != x):
			nn_y = v_i_y;
			nn_x = v_i_x;
			visited[v_i_x][v_i_y] = 1;
			y_vector[vector_index_counter] = nn_y;
			x_vector[vector_index_counter] = nn_x;
			vector_index_counter += 1;
			nn_found = True;
			y = nn_y;
			x = nn_x;
			window_size = 3;
			break;
	if (not nn_found):
		window_size += 2;
	if (vector_index_counter - 1 == number_of_pixels or window_size > image_reduced.shape[0]):
		break;

# Trimming coordinates of zeros
numpy.trim_zeros(y_vector);
numpy.trim_zeros(x_vector);

print(y_vector);

# Plotting route
print('Plotting route...\n');
for vi in range(0, x_vector.shape[0] - 1):
	cv2.line(image_reduced, (y_vector[vi], x_vector[vi]), (y_vector[vi + 1], x_vector[vi + 1]), (0, 255, 0), 1);

cv2.imshow('Traveler', image_reduced);
cv2.waitKey();
