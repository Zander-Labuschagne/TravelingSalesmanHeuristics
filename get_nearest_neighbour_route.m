function [x_list_trimmed, y_list_trimmed] = get_nearest_neighbour_route(image_plot, number_of_cities)
%GET_NEAREST_NEIGHBOUR_ROUTE Summary of this function goes here
%   Detailed explanation goes here
%% Find nearest neighbours
	fprintf('Finding nearest neighbours...\n');
	window_size = 3;
	visited = zeros(size(image_plot, 1), size(image_plot, 2));
	list_counter = 1;
	x_list = zeros(1, number_of_cities);
	y_list = zeros(1, number_of_cities);
	[x, y] = find_first_city(image_plot);
	%x_list(1) = x
	%y_list(1) = y
	while (true)
		nn_found = false;
		for iv = (y - floor(window_size / 2)) : y + floor(window_size / 2)
			for v = x - floor(window_size / 2) : x + floor(window_size / 2)
				%% Ensuring window is within image bounds for both axis
				if (iv < 1)
					iv_i = 1;
				elseif (iv > size(image_plot, 2))
					iv_i = size(image_plot, 2);
				else
					iv_i = iv;
				end;
				if (v < 1)
					v_i = 1;
				elseif (v > size(image_plot, 1))
					v_i = size(image_plot, 1);
				else
					v_i = v;
				end;

				%% If neighbour is found
				if (image_plot(v_i, iv_i) == 0 && visited(v_i, iv_i) == 0 && iv_i ~= y && v_i ~= x)
					%fprintf('Neighbour found!\n');
					nn_y = iv_i;
					nn_x = v_i;
					visited(nn_x, nn_y) = 1;

					% add line to list of lines to draw later
					y_list(1, list_counter) = nn_y;
					x_list(1, list_counter) = nn_x;

					list_counter = list_counter + 1;

					nn_found = true;
					y = nn_y;
					x = nn_x;
					window_size = 3;
					break;
				end;
			end;
			if (nn_found)
				break;
			end;
		end;
		if (nn_found == false)
			window_size = window_size + 2;
		end;
		if (list_counter - 1 == number_of_cities || window_size > size(image, 1))
			break;
		end;
	end;

	%% Trimming coordinates of zeros
	x_list_trimmed = x_list(find(x_list, 1, 'first'):find(x_list, 1, 'last'));
	y_list_trimmed = y_list(find(y_list, 1, 'first'):find(y_list, 1, 'last'));
end

